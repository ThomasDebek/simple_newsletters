Rails.application.routes.draw do
  root 'welcomes#index'

  get 'welcomes/show'

  #get 'leads/index'
  resources :leads
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
