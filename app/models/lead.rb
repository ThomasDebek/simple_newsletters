class Lead < ApplicationRecord
  validates :name, :email, :device, presence: true
end
