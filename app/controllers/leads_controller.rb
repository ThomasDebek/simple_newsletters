class LeadsController < ApplicationController
  def index
  end

  def create
    @lead = Lead.new(lead_params)
    if @lead.save
      cookies[:saved_lead] = true
      redirect_to welcomes_show_path, action: "show"
      flash[:notice] = "Successfuly created "
    else
      flash[:alert] = "Something was wrong, please try again"

    end
  end

  private
  def lead_params
    params.require(:lead).permit(:name, :email, :device, :country )
  end

end

